﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class noteGen : MonoBehaviour
{
    public int[] midiNotes;

    private int noteIndex;
    private int noteGenerated;
    private int octaveRandomChoice;
    private int octaveFinal;

    private int testInt = 0;

    [Range(-3, 3)]
    public int octaveOffset = 0;
    public bool createNote = false;

    public void Update()
    {
        if (createNote)
        {
            randomIndex();
            randomOctave();
            octaveFix();
            noteMake();
            Print();
            createNote = false;
        }
    }

    // chooses a random index for the musical scale created on editor
    public void randomIndex()
    {
        noteIndex = Random.Range(0, midiNotes.Length);
    }

    // picks a random integer to choose down, middle, or up an octave
    public void randomOctave()
    {
        octaveRandomChoice = Random.Range(-1, 2) * 12;
    }

    // sets initial offset to note number 48 and can be offset further octaves in editor
    public void octaveFix()
    {
        octaveFinal = (octaveOffset * 12) + octaveRandomChoice + 48;
    }

    // adds random octave and editor octave offset to randomly generated scale note
    public void noteMake()
    {
       noteGenerated = octaveFinal + midiNotes[noteIndex];
    }

    public void Print()
    {
        Debug.Log("note make = " + noteGenerated);
    }

    public void Start()
    {
        //loop();
    }


    public void loop()
    {
        // testInt = testInt - 1;
        // Debug.Log("Value = " + testInt + "    Scale Value = " + testInt % midiNotes.Length + "    Octave Value = " + testInt / 12);

       

        Debug.Log("note make = " + noteGenerated);

        Invoke("loop", 0.5f);
    }
}
