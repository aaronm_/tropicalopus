﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnPollenNotes : MonoBehaviour
{
    public GameObject noteGrainPrefab;

    public Vector3 center;
    public Vector3 size;
    public int SequenceNumber; //This is used for saving to gallery
    public int amountOfNoteGrains;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < amountOfNoteGrains; i++)
        {
            spawnNoteGrain(i);
        }

    }

    public void spawnNoteGrain(int i)
    {
        Vector3 pos = center + new Vector3(Random.Range(-size.x / 2, size.x / 2), Random.Range(-size.y / 2, size.y / 2), Random.Range(-size.z / 2, size.z / 2));

        var myNewNoteGrain = Instantiate(noteGrainPrefab, pos, Quaternion.identity);
        myNewNoteGrain.gameObject.name = myNewNoteGrain.gameObject.name + i;
        myNewNoteGrain.GetComponent<grainPrefabHandler>().note_position = i;
        myNewNoteGrain.transform.parent = gameObject.transform;
    }

    void OnDrawGizmosSelected()

    {
        Gizmos.color = new Color(1, 0, 0, 0.2f);
        Gizmos.DrawCube(center, size);
    }

}
