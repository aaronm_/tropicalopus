﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class noteSceneManager : MonoBehaviour
{
    public noteManager sq1;
    public noteManager sq2;
    public noteManager sq3;

    void Start()
    {
        sq1.initiate();
        sq2.initiate();
        sq3.initiate();
        // initiate other noteManagers for each sequencer here
    }

}
