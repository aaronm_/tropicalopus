﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.UI;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.SceneManagement;

public class PO_SceneManager : MonoBehaviour
{
    public TrackedImageInfoManager trackedImageInfoManager;
    private string current_name = "nothing yet";
    // Start is called before the first frame update
    void Start()
    {
        trackedImageInfoManager.m_TrackedImageManager.trackedImagesChanged += weHaveLiftOff;
    }

    void drawScene(ARTrackedImage trackedImage)
    {
        var canvas = trackedImage.GetComponentInChildren<Canvas>();
            canvas.worldCamera = trackedImageInfoManager.worldSpaceCanvasCamera;

            // Update information about the tracked image
            var text = canvas.GetComponentInChildren<Text>();
            text.text = string.Format(
                "{0}\n AYYYYYYYYYYYYYY",
                trackedImage.referenceImage.name);

            var planeParentGo = trackedImage.transform.GetChild(0).gameObject;
            var planeGo = planeParentGo.transform.GetChild(0).gameObject;

            // Disable the visual plane if it is not being tracked
            if (trackedImage.trackingState != TrackingState.None)
            {
                planeGo.SetActive(true);

                // The image extents is only valid when the image is being tracked
                trackedImage.transform.localScale = new Vector3(trackedImage.size.x, 1f, trackedImage.size.y);

                // Set the texture
                var material = planeGo.GetComponentInChildren<MeshRenderer>().material;
                material.mainTexture = (trackedImage.referenceImage.texture == null) ? trackedImageInfoManager.defaultTexture : trackedImage.referenceImage.texture;
            }
            else
            {
                planeGo.SetActive(false);
            }
    }

    void weHaveLiftOff(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (var trackedImage in eventArgs.added)
        {
            // if (trackedImage.referenceImage.name == current_name)
            // {
            //     return;
            // }
            // else
            // {
            //     current_name == trackedImage.referenceImage.name;
            // }
            // Give the initial image a reasonable default scale
            trackedImage.transform.localScale = new Vector3(0.0003f, 0.0003f, 0.0003f) / 200000;
            drawScene(trackedImage);
        }

        // foreach (var trackedImage in eventArgs.updated)
        //     drawScene(trackedImage);
    }
}
