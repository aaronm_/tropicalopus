﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioHelm;

public class EnvelopeManager : MonoBehaviour
{
    public bool EnvelopeToZero = false;
    public HelmSequencer hs;
    //private bool reset = false;
    //private CommonParam envelope;

    void Start() {
        //envelope = CommonParam.kFilterEnvelopeDepth;
    }

    // Update is called once per frame
    void Update()
    {
        if (EnvelopeToZero) {
            Native.HelmSilence(1, true);
            Native.HelmSetParameterValue(1, 2, 0);
            //CommonParam.kFilterEnvelopeDepth = 0;
            EnvelopeToZero = false;
            //reset = true;
        }
        // if (reset) {
        //     CommonParam.kFilterEnvelopeDepth = envelope;
        //     reset = false;
        // }
    }
}
