﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadPlaqueRows : MonoBehaviour
{
    /* Script to load the UI based on the number of plaques we have completed, debug for now as we aren't storing data */
    public int rows = 3;
    public int columns = 3;
    public GameObject playButtonPrefab;
    public AudioHelm.HelmSequencer[] helmSequencers;
    private List<List<DebugMusicItem>> plaqueMusicList; //Don't know how it's going to be stored yet, probably just ints in a csv or json?
    private List<GameObject> playButtons;
    private Vector3 localScale = new Vector3(1,1,1);
    private const int MAX_NOTES_PER_SEQUENCE = 5;
    [SerializeField]
    private GridLayoutGroup Grid;

    void Start()
    {
        plaqueMusicList = new List<List<DebugMusicItem>>();
        playButtons = new List<GameObject>();
        buildUI();
    }

    public void playMusicItem(int rowNumber, int columnNumber)
    {
        DebugMusicItem item = plaqueMusicList[rowNumber][columnNumber];
        Debug.Log(string.Format("Chose to play row number {0}, column number {1}",rowNumber, columnNumber));
        Debug.Log(string.Format(item.play(), rowNumber, item.ItemColumnPosition));
        readStoredPlaqueMusicFromFile(rowNumber);
    }

    private void debugPlayMultipleMusicItems(List<RowCoords> musicItems)
    {
        foreach (var musicItem in musicItems)
        {
            playMusicItem(musicItem.rowNumber, musicItem.columnNumber);
        }
    }

    public void playRandomMusicItems(int numberOfRandomitems)
    {
        List<RowCoords> itemsToPlay = chooseRandomMusicItems(numberOfRandomitems);
        //not sure how playing will work in the end just debugging the random choice for now
        debugPlayMultipleMusicItems(itemsToPlay);
    }

    private List<RowCoords> chooseRandomMusicItems(int numberOfRandomitems)
    {
        List<RowCoords> itemsToPlay = new List<RowCoords>();
        for(int i = 0; i < numberOfRandomitems; i++)
        {
            RowCoords itemPosition = new RowCoords();
            itemPosition.rowNumber = Random.Range(0, plaqueMusicList.Count - 1);
            itemPosition.columnNumber = Random.Range(0,plaqueMusicList[itemPosition.rowNumber].Count - 1);
            itemsToPlay.Add(itemPosition);
        }
        return itemsToPlay;
    }

    private void readStoredPlaqueMusicFromFile(int rowNo)
    { 
        float[] noteVals = getNoteValue(rowNo+1); //first sequence
        float[] noteTimes = getNoteTiming(rowNo+1); //first sequence
        for (int i = 0; i < MAX_NOTES_PER_SEQUENCE; i++)
        {
            if (noteVals[i] != 0.0f && noteTimes[i] != 0.0f)
            {
                Debug.Log(string.Format("HALLO"));
                helmSequencers[rowNo].AddNote((int)noteVals[i], noteTimes[i], noteTimes[i] + 1);
            }
        }
    }

    private float[] getNoteData(int sequence, string data, string plant = "plant")
    {
        float[] output = new float[MAX_NOTES_PER_SEQUENCE];
        for (int noteNum = 0; noteNum < MAX_NOTES_PER_SEQUENCE; noteNum++)
        {
            output[noteNum] = PlayerPrefs.GetFloat(plant + "-" + sequence + "-" + noteNum + "-" + data);
        }
        return output;
    }

    private float[] getNoteValue(int sequence, string plant = "plant")
    {
        return getNoteData(sequence, "value", plant);
    }

    private float[] getNoteTiming(int sequence, string plant = "plant")
    {
        return getNoteData(sequence, "time", plant);
    }

    private void buildUI()
    {
        for (int i = 0; i < rows; i++)
        {
            plaqueMusicList.Add(new List<DebugMusicItem>());
            for(int y = 0; y < columns; y++)
            {    
                plaqueMusicList[i].Add(new DebugMusicItem(y));
                addButtonToUIGridForMusicItem(i,y);
            }
        }
    }
 
    private void addButtonToUIGridForMusicItem(int rowPos, int columnPos)
    {
        if (playButtonPrefab == null)
            return;
        GameObject playButton = GameObject.Instantiate(playButtonPrefab);
        playButton.transform.SetParent(Grid.transform, false);
        playButton.transform.localScale = localScale;
        playButton.transform.localPosition = Vector3.zero;
        PlayButton buttonScript = playButton.GetComponent<PlayButton>();
        buttonScript.gridPositionX = columnPos;
        buttonScript.gridPositionY = rowPos;
        buttonScript.controller = this;
        playButtons.Add(playButton);
    }

    private struct RowCoords
    {
        public int rowNumber;
        public int columnNumber;
    }

    protected class DebugMusicItem
    {
        public DebugMusicItem(int columnPos)
        {
            ItemColumnPosition = columnPos;
        }
        public int ItemColumnPosition { get; } = -1;
        public string play(){ return "playing test music element in {0}{1}";}
    }
}