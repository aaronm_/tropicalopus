﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioHelm;

public class grainPrefabHandler : MonoBehaviour
{
    public HelmSequencer helmSequencer;
    public noteManager myNoteManager;
    public bool turnon = false;
    int note;
    public int length = 1;
    public int note_position; //Used to save to gallery
    // Start is called before the first frame update


void Start()
    {
        helmSequencer = GetComponentInParent<HelmSequencer>();
        helmSequencer.AllNotesOff();
        // Native.HelmSilence(1, false);
        // Native.HelmSilence(2, false);
        // Native.HelmSilence(3, false);
    }

    void noteHandler()
    {
        //Method to be called to handle notes - avoid duplicating code in the update method for both touchscreen and debug
        myNoteManager.action();
        note = myNoteManager.midiValue;
        helmSequencer.NoteOn(note);
        helmSequencer.AddNote(note, seq(), len(length));
        Debug.Log(seq());
        Destroy(this.gameObject);
        turnon = false;
        var seqq = GetComponentInParent<spawnPollenNotes>().SequenceNumber;
        PlayerPrefs.SetFloat("plant-"+seqq+"-"+note_position+"-time", (float)helmSequencer.GetSequencerPosition()*helmSequencer.GetSixteenthTime());
        PlayerPrefs.SetFloat("plant-"+seqq+"-"+note_position+"-value", note);
    }

    // Update is called once per frame
    void Update()
    {
		if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began)) //This is used for checking object is touched on phone
		{
			Ray raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
			RaycastHit raycastHit;
			if (Physics.Raycast(raycast, out raycastHit))
			{
				if (raycastHit.collider.name == this.gameObject.name)
				{
                    noteHandler();
				}
			}
		}

        if (turnon)
        {
            noteHandler();
        }
    }

    float seq()
    {
        return (int)helmSequencer.GetSequencerPosition();
    }

    float len(int len=1)
    {
        return seq() + len;
    }
}
