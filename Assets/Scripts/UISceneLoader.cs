using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class UISceneLoader : MonoBehaviour
{
    public string scene;
    public void LoadScene() {
        SceneManager.LoadScene(scene);
    }
}