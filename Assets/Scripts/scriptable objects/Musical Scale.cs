﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "scale-", menuName = "Musical Scale")]

public class MusicalScale : ScriptableObject
{

    public int[] midiNotes;

    private int oct = 12;

    [Range(-3, 3)]
    public int octaveOffset = 0;

    public bool createNote = false;

    public void Update()
    {
         if(createNote = true)
        {
            Print();
            createNote = false; 
        }
    }

    public void Print()
    {
     Debug.Log(midiNotes[1]);
    }

}