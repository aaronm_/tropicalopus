﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class splashLoader : MonoBehaviour
{
    public int sceneIndex = 1;
    public int loadTime = 5;
    public bool enableTimer = false;

    void Start()
    {
        if (enableTimer == true)
        {
            Invoke("sceneStart", loadTime);
        }
    }

    public void sceneStart()
    {
        SceneManager.LoadScene(sceneIndex);
        //SceneManager.LoadScene("OtherSceneName", LoadSceneMode.Additive);
    }
}
