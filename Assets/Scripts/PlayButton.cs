﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButton : MonoBehaviour
{
    public LoadPlaqueRows controller;
    public int gridPositionX {get;set;}
    public int gridPositionY {get;set;}

    public void playButtonClicked()
    {
        controller.playMusicItem(gridPositionX, gridPositionY);
    }
}
