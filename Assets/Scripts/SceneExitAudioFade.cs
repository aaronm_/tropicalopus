﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using AudioHelm;

public class SceneExitAudioFade : MonoBehaviour
{
    public AudioSource[] audioSource;
    public HelmSequencer[] helmSequencer;
    public string sceneName;
    public HelmController controller;

    void Awake()
    {
        SceneManager.sceneLoaded += (Scene scene, LoadSceneMode mode) =>{
            //change back to origin patch - dont know what that is so trying this one
            HelmPatch originPatch = new HelmPatch();
            originPatch.LoadPatchData("Assets/AudioHelm/Presets/Keys/CM Bells.helm");
            controller.LoadPatch(originPatch);
            //set master synth volume to 1
            /* Native.HelmSetParameterValue(1, (int)Param.kVolume, 1);
            Native.HelmSetParameterValue(2, (int)Param.kVolume, 1);
            Native.HelmSetParameterValue(3, (int)Param.kVolume, 1);*/

            Debug.Log("NORMAL VOLUME APPLIED");
        };
    }

    public void callFadeout() 
    {
        //set master synth volume to zero
        Native.HelmSetParameterValue(1, (int)Param.kVolume, 0);
        Native.HelmSetParameterValue(2, (int)Param.kVolume, 0);
        Native.HelmSetParameterValue(3, (int)Param.kVolume, 0);
        //change to reset patch
        HelmPatch resetPatch = new HelmPatch();
        resetPatch.LoadPatchData("Assets/AudioHelm/Presets/TRAR/TRAR-reset.helm");
        controller.LoadPatch(resetPatch);

        Debug.Log("RESET APPLIED");
        SceneManager.LoadScene(sceneName);
    }
}
