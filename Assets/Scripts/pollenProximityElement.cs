﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pollenProximityElement : MonoBehaviour
{
    //Distance below which the ui element turns on
    public float highlightDistance = 1;
    public GameObject uiElement;
    private bool insideFOV = false;

    private void Awake()
    {
        if (uiElement != null)
        {
            uiElement.GetComponent<Canvas>().worldCamera = Camera.main;
        }
    }

    void Update()
    {
        if(insideFOV && highlightDistance > Vector3.Distance(this.transform.position, Camera.main.transform.position))
        {
            uiElement?.SetActive(true);
            uiElement?.transform.LookAt(Camera.main.transform);
        }
        else
        {
            uiElement?.SetActive(false);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.name == "FOVCylinder")
        {
            insideFOV = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.name == "FOVCylinder")
        {
            insideFOV = false;
        }
    }
}
