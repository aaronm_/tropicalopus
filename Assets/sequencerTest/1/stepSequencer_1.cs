﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioHelm;

public class stepSequencer_1 : MonoBehaviour
{
    public SampleSequencer thisSequencer;
    public bool[] step;
    public int note;


    void Start()
    {
        int res = 4;

        thisSequencer = GetComponentInParent<SampleSequencer>();
        thisSequencer.AllNotesOff();
        thisSequencer.length = step.Length * res;
    }

    void Update()
    {
        if (Input.GetKeyUp("space"))
        {
            seqAction();
        }
    }

    public void seqAction()
    {
        for (int i = 0; i < step.Length; i++)
        {
            if(step[i])
            {
                noteAdded(note, pos(i), leng(i));
            }
            else
            {
                noteRemoved(note, pos(i), leng(i));
            }
        }
    }

    //float seq()
    //{
    //    return (int)thisSequencer.GetSequencerPosition();
    //}

    int leng(int st)
    {
        int res = 4;
        return (st * res) + 1;
    }

    int pos(int st)
    {
        int res = 4;
        return (st * res);
    }

    void noteAdded(int nt, float pos, float len)
    {
        thisSequencer.AddNote(nt, pos, len);
    }

    void noteRemoved(int nt, float pos, float len)
    {
        thisSequencer.RemoveNotesInRange(nt, pos, len);
    }
}
