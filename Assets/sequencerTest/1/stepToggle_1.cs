﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stepToggle_1 : MonoBehaviour
{
    public bool active = false;

    public stepSequencer_1 seq;

    public int stepIndex;



    Renderer stepRend;

    void Start()
    {
        Color col = new Color();
        GetComponent<Renderer>().material.color = col;

        stepRend = GetComponent<Renderer>();
        stepRend.material.SetColor("_Color", Color.red);
    }

    void OnMouseDown()
    {
        active = !active;

        if(active == true)
        {
            transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
            seq.step[stepIndex] = true;
            seq.seqAction();

            stepRend.material.SetColor("_Color", Color.green);
        }
        else
        {
            transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f);
            seq.step[stepIndex] = false;

            seq.seqAction();

            stepRend.material.SetColor("_Color", Color.red);

        }
    }
}
