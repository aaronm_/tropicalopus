﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stepSetup : MonoBehaviour
{
    // values for rows and step amounts
    public int amountOfSteps;
    public int noteRowAmount;

    // enum for setting resolution of step sequencer
    public enum gridResolution { t4, t8, t16 };
    public gridResolution _gridResolution;
    [HideInInspector]
    public int gridResoFactor;

    // values for changing scale and spacing of step toggles
    public float xSpacing = 1.0f;
    public float ySpacing = 1.0f;
    public float buttonScale = 1.0f;

    // prefab slots for step toggles and parent note rows
    public GameObject child;
    public GameObject noteRowParent;

    // sets the MIDI note number for consecutive note rows, this array size should at least equal the noteRowAmount int
    public int[] NoteChoices;

    // declaration of 
    stepToggle_2 stepToggle;

    void Awake()
    {
        resSwitch();
    }

    void Start()
    {
        // this embedded double for loop generates the amount of note rows and steps based on the editor choice
        // it also sets the transform position of the note prefabs, and allocates row indexes, position indexes & MIDI note values

        for (int i = 0; i < noteRowAmount; i++)
        {
            var vertOffset = i * -ySpacing;
            var obj = Instantiate(noteRowParent, new Vector3(0, vertOffset, 0), Quaternion.identity, transform);
            obj.name = "Row - " + (i+1);

            for (int x = 0; x < amountOfSteps; x++)
            {
                var horOffset = x * xSpacing;

                stepToggle = child.GetComponent<stepToggle_2>();

                stepToggle.noteRow = i;
                stepToggle.stepIndex = x;
                stepToggle.midiNote = NoteChoices[i];

                var objStep = Instantiate(child, new Vector3(horOffset, vertOffset, 0), Quaternion.identity, obj.transform);
                objStep.transform.localScale = new Vector3(buttonScale, buttonScale, buttonScale);

                objStep.name = "Step - " + (x + 1);
            }
        }
    }

    // this switch is just for out putting an int value based on the grid resolution drop down
    void resSwitch()
    {
        switch (_gridResolution)
        {
            case gridResolution.t4:
                gridResoFactor = 4;
                break;
            case gridResolution.t8:
                gridResoFactor = 8;
                break;
            case gridResolution.t16:
                gridResoFactor = 16;
                break;
            default:
                print("res switch has broken.");
                break;
        }
    }
}
