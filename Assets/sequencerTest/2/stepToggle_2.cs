﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stepToggle_2 : MonoBehaviour
{
    public bool active = false;

    public stepSequencer_2 seq;

    public int stepIndex;
    public int noteRow;
    public int midiNote;

    Renderer stepRend;

    void Start()
    {
        Color col = new Color();
        GetComponent<Renderer>().material.color = col;

        // gets stepSequencer in parent of parent
        seq = transform.parent.parent.GetComponent<stepSequencer_2>();

        stepRend = GetComponent<Renderer>();
        stepRend.material.SetColor("_Color", Color.red);
    }

    void OnMouseDown()
    {
        active = !active;

        if(active == true)
        {
            seq.steps2d[stepIndex, noteRow] = true;
            seq.seqAction(midiNote, noteRow);

            visualOn();
        }
        else
        {
            seq.steps2d[stepIndex, noteRow] = false;
            seq.seqAction(midiNote, noteRow);

            visualOff();
        }
    }

    void visualOn()
    {
        transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
        stepRend.material.SetColor("_Color", Color.green);
    }

    void visualOff()
    {
        transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f);
        stepRend.material.SetColor("_Color", Color.red);
    }
}
