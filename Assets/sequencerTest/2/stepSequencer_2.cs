﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioHelm;

public class stepSequencer_2 : MonoBehaviour
{
    // for connecting to main parent sequencer
    // for pulling info about step sequencer init
    // declaring 2d array for steps and rows
    SampleSequencer thisSequencer;
    stepSetup getSetupInfo;
    public bool[,] steps2d;

    int stepResOffset;

    void Start()
    {
        // connects to parent sequencer and inits all notes off
        thisSequencer = GetComponentInParent<SampleSequencer>();
        thisSequencer.AllNotesOff();

        // pulls setup component for extracting init info
        // sets 2d array based on step and row information set on stepSetup
        getSetupInfo = GetComponent<stepSetup>();
        steps2d = new bool[getSetupInfo.amountOfSteps, getSetupInfo.noteRowAmount];

        // Sets the length of the master sequencer based on the stepSetup settings
        // Length = (16 divided by grid resolution) * amount of steps
        thisSequencer.length = (16/getSetupInfo.gridResoFactor) * getSetupInfo.amountOfSteps;

        // Sets the position offset for each step to be written into the main seq, used by leng & pos methods
        stepResOffset = (16 / getSetupInfo.gridResoFactor);
    }

    public void seqAction(int note, int noteRow)
    {
        for (int i = 0; i < steps2d.GetLength(0); i++)
        {
            if(steps2d[i, noteRow])
            {
                noteAdded(note, pos(i), leng(i));
            }
            else
            {
                noteRemoved(note, pos(i), leng(i));
            }
        }
    }

    int leng(int seqActionLeng)
    {
        return (seqActionLeng * stepResOffset) + 1;
    }

    int pos(int seqActionPos)
    {
        return (seqActionPos * stepResOffset);
    }

    void noteAdded(int nt, float pos, float len)
    {
        thisSequencer.AddNote(nt, pos, len);
    }

    void noteRemoved(int nt, float pos, float len)
    {
        thisSequencer.RemoveNotesInRange(nt, pos, len);
    }

    //float seq()
    //{
    //    return (int)thisSequencer.GetSequencerPosition();
    //}
}