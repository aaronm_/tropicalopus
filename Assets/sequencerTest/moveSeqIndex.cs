﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioHelm;

public class moveSeqIndex : MonoBehaviour
{
    public SampleSequencer Seq;
    public stepSetup StepInfo;

    public double Pos;
    public int PosWrapped;
    float ScaleX;
    int resScaler;

    void Start()
    {
        StepInfo = GetComponentInParent<stepSetup>();
        ScaleX = StepInfo.xSpacing;

        resScaler = (16 / StepInfo.gridResoFactor);
    }

    void Update()
    {
        Pos = Seq.GetSequencerPosition();

        PosWrapped = (int)Pos;
        PosWrapped = PosWrapped / resScaler;

        transform.position = new Vector3((PosWrapped) * ScaleX, 1.0f, 0.0f);
    }
}
