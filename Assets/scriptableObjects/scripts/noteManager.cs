﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Note Manager", menuName = "Note Manager")]
public class noteManager : ScriptableObject
{

    public scale loadedScale;
    int scaleSize;

    public int scaleIndexWrapped = 0;
    [Tooltip("Make sure this value is within the scale length")]
    public int intervalInit = 0;

    // add -3 +3 slider param
    public int octaveInit = 5;
    int octave = 0;

    int octaveOffsetAsSemitones;

    int midiValueInit = 60;
    public int midiValue;
    public int selectedInterval;

    public int trueCount;
    int initTrueCount;
    int initTrueCountOct;
    int upOrDownValue;

    // variables to help determine octave range for randomisation and drunk bounds
    int octaveRange;
    int randomOffset;
    int oneOctBelowTrueCount;

    public bool nudgeRandom = false;
    bool firstNotePlayed;

    //void Start()
    //{
    //    initiate();
    //    //noteTrigger();
    //}

    //void Update()
    //{
    //        if (nudgeRandom == true)
    //        {
    //            action();
    //            nudgeRandom = false;                
    //        }        
    //}

    public void action()
    {
        // checks to see if the first note has been played, if it has it does a deviate before playing a note
        // if it hasn't played, it triggers the initial note value
        if (firstNotePlayed)
        {
            deviate();
        }
        else 
        {
            noteTrigger();
        }

        firstNotePlayed = true;
    }

    public void initiate()
    {
        //pulls in scale array size for wrapping, 
        scaleSize = loadedScale.notes.Length;
        //sets trueCount arbirarily large, but also a multiple of the scale to correct wrapping value
        //this probably could be a value between 0-127 but needs to have offset values thought about
        trueCount = scaleSize * 1000;
        trueCount += intervalInit;

        //sets wrapped index for first note trigger
        scaleIndexWrapped = trueCount % scaleSize;

        // remembers initial true count value for offseting in later calcs - see random note method
        initTrueCount = trueCount;

        initTrueCountOct = trueCount / scaleSize;

        // determines a range of 2 octaves for trueCount to randomly pick from
        octaveRange = scaleSize * 2;

        // picks initial starting count value and drops an octave for base calculation
        oneOctBelowTrueCount = initTrueCount - scaleSize;

        // sets first note played to false so system can play starting note position when needed
        firstNotePlayed = false;

        setOctave();
        setInterval();
        setMidiValue();
    }

    // deviate uses a probability to pick either a drunk movement up or down, or a random choice
    void deviate()
    {
        float x = Random.Range(0, 99);

        // extra conditions here can determine whatt function is called, including pass parameter to drunk
        if (x < 70.0f)
        {
            drunk();
        }
        else {
            randomised(); 
        }
    }

    void randomised()
    {
        // Passes 2, lets the method know its a random operation, then sets the true count master via ?
        setTrueCount(2);
        setOctave();
        setInterval();
        setMidiValue();
        noteTrigger();
    }

  
    void drunk()
    {
        // picks a random value to determine choice
        float x = Random.Range(0, 99);

        // with a 50/50 probability chooses either up or down
        if (x < 35)
        {
            upOrDownValue = -1;
        }
        else 
        {
            upOrDownValue = 1;
        }

        // Passes either -1 or 1, letting the method know its a drunk operation, then sets the true count master value up or down 
        setTrueCount(upOrDownValue);

        // function order below is very important, notes for each function listed below
        // in order to provide random functionality, I might need create a separate trueCount function
        setOctave();
        setInterval();
        setMidiValue();
        noteTrigger();
    }

    public void setTrueCount(int choice)
    {
        if (choice < 2)
        {
            // adds an up for down value to the master tracked true count variable
            trueCount += upOrDownValue;
            // wraps the master value into a note scale selection from the scale array length
            scaleIndexWrapped = trueCount % scaleSize;

            // if trueCount is out of bounds - beyond -1 or +1 octave on starting interval, it randomises trueCount instead 
            // oneOct below is trueCountInit - one scale length, or condition adds x2 octaves on
            if (trueCount < oneOctBelowTrueCount || trueCount > (oneOctBelowTrueCount + octaveRange))
            {
                setTrueCount(2);
            }

            Debug.Log("Drunk triggered with a value of = " + upOrDownValue);
        }
        else 
        {
            // if a 2 is passed through the method it does a random note selection

            // picks a random value for trueCount 
            randomOffset = Random.Range(0, octaveRange);

            // sets new trueCount variable between -1 and + 1 octaves of it's starting value
            trueCount = oneOctBelowTrueCount + randomOffset;
            scaleIndexWrapped = trueCount % scaleSize;

            Debug.Log("Rndm - offset = " + randomOffset + "POS should be =  " + trueCount);

        }
    }


    void setOctave()
    {
        // this works out its' relative octave position with a baseline of zero
        octave = (trueCount / scaleSize) - initTrueCountOct;
        // and then adds the octave initial value to bring itself back into the proper position
        octave += octaveInit; 
    }

    void setInterval()
    {
        // sets the interval value based on the scale and whatever the wrapped value chooses for an index
        selectedInterval = loadedScale.notes[scaleIndexWrapped];
    }

    void setMidiValue()
    {
        // this is the final value to be chosen for the sequencer
        // temp var figures out the MIDI value for the base octave selection
        // then it adds the interval choice on top to give the true MIDI note number
        int x = octave * 12;
        midiValue = x + selectedInterval;
    }

    void noteTrigger()
    {
        Debug.Log("POS : " + trueCount + " OCT : " + octave + " MIDI : " + midiValue + " INTv : " + selectedInterval + " WRP : " + scaleIndexWrapped);
    }
}
